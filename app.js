require('dotenv').load();

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');

require('./app_api/models/db');
require('./app_api/konfiguracija/passport');

var uglifyJs = require('uglify-js');
var fs = require('fs');

//var index = require('./app_server/routes/index');
var indexApi = require('./app_api/routes/index');
var users = require('./app_server/routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'jade');

var zdruzeno = uglifyJs.minify({
  'app.js': fs.readFileSync('app_client/app.js', 'utf8'),
  'seznam.krmilnik.js': fs.readFileSync('app_client/seznam/seznam.krmilnik.js', 'utf8'),
  'informacije.krmilnik.js': fs.readFileSync('app_client/informacije/informacije.krmilnik.js', 'utf8'),
  'podrobnostiLokacije.krmilnik.js': fs.readFileSync('app_client/podrobnostiLokacije/podrobnostiLokacije.krmilnik.js', 'utf8'),
  'komentarModalnoOkno.krmilnik.js': fs.readFileSync('app_client/komentarModalnoOkno/komentarModalnoOkno.krmilnik.js', 'utf8'),
  'registracija.krmilnik.js': fs.readFileSync('app_client/avtentikacija/registracija/registracija.krmilnik.js', 'utf8'),
  'prijava.krmilnik.js': fs.readFileSync('app_client/avtentikacija/prijava/prijava.krmilnik.js', 'utf8'),
  'geolokacija.storitev.js': fs.readFileSync('app_client/skupno/storitve/geolokacija.storitev.js', 'utf8'),
  'edugeocachePodatki.storitev.js': fs.readFileSync('app_client/skupno/storitve/edugeocachePodatki.storitev.js', 'utf8'),
  'avtentikacija.storitev.js': fs.readFileSync('app_client/skupno/storitve/avtentikacija.storitev.js', 'utf8'),
  'formatirajRazdaljo.filter.js': fs.readFileSync('app_client/skupno/filtri/formatirajRazdaljo.filter.js', 'utf8'),
  'dodajHtmlPrehodVNovoVrstico.filter.js': fs.readFileSync('app_client/skupno/filtri/dodajHtmlPrehodVNovoVrstico.filter.js', 'utf8'),
  'prikaziOceno.direktiva.js': fs.readFileSync('app_client/skupno/direktive/prikaziOceno/prikaziOceno.direktiva.js', 'utf8'),
  'noga.direktiva.js': fs.readFileSync('app_client/skupno/direktive/noga/noga.direktiva.js', 'utf8'),
  'navigacija.direktiva.js': fs.readFileSync('app_client/skupno/direktive/navigacija/navigacija.direktiva.js', 'utf8'),
  'navigacija.krmilnik.js': fs.readFileSync('app_client/skupno/direktive/navigacija/navigacija.krmilnik.js', 'utf8'),
  'glava.direktiva.js': fs.readFileSync('app_client/skupno/direktive/glava/glava.direktiva.js', 'utf8')
});

fs.writeFile('public/angular/edugeocache.min.js', zdruzeno.code, function(napaka) {
  if (napaka)
    console.log(napaka);
  else
    console.log('Skripta je zgenerirana in shranjena v "edugeocache.min.js"');
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(passport.initialize());

//app.use('/', index);
app.use('/api', indexApi);
app.use('/users', users);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

// Obvladovanje napak zaradi avtentikacije
app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"sporočilo": err.name + ": " + err.message});
  }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
