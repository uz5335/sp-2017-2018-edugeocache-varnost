var mongoose = require('mongoose');

var komentarjiShema = new mongoose.Schema({
    avtor: {type: String, required: true},
    ocena: {type: Number, required: true, min: 0, max: 5},
    besediloKomentarja: {type: String, required: true},
    datum: {type: Date, "default": Date.now}
});
  
var delovniCasiShema = new mongoose.Schema({
    dnevi: {type: String, required: true},
    odprtje: String,
    zaprtje: String,
    zaprto: {type: Boolean, required: true}
});

var lokacijeShema = new mongoose.Schema({
    naziv: {type: String, required: true},
    naslov: String,
    ocena: {type: Number, "default": 0, min: 0, max: 5},
    lastnosti: [String],
    koordinate: {type: [Number], index: '2dsphere', required: true},
    delovniCas: [delovniCasiShema],
    komentarji: [komentarjiShema]
});

mongoose.model('Lokacija', lokacijeShema, 'Lokacije');